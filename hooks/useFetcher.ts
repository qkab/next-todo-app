async function useFetcher<T>({url, token }: { url: string, token: string }): Promise<T> {
  const res = await fetch(url, { headers: { 'Authorization': `Bearer ${token}` }})
  return res.json()
}

export default useFetcher