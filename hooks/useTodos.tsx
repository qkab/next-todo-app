import { useMemo } from 'react'
import useSWR from 'swr'
import { ITodo } from '../interfaces/todos.interface'

function useTodos(token: string | null) {
  const options = useMemo(() => ({ url: '/api/todos', token }), [token])

  const fetcher = async (url: string) => {
    const res = await fetch(url, { headers: { 'Authorization': `Bearer ${token}` }})
    
    if(!res.ok) {
      const error: any = new Error('An error occurred while fetching the data.')
      // Attach extra info to the error object.
      error.status = res.status
      throw error
    }
    return await res.json()
  }

  const { data, error } = useSWR<ITodo[]>(token ? options.url : null, fetcher)

  return { todos: data, error }
}

export default useTodos