import jwtDecode from 'jwt-decode';
import useAuth from './useAuth';
import { UserState } from '../contexts/user.context';
import { renderHook } from '@testing-library/react-hooks';

jest.mock('jwt-decode', () => {
  const USER_TOKEN_DECODED_PAYLOAD: UserState = {
    token: 'TOKEN',
    email: 'EMAIL',
    firstname: 'FIRSTNAME',
    lastname: 'LASTNAME'
  }
  return jest.fn().mockReturnValue(USER_TOKEN_DECODED_PAYLOAD)
})

describe(useAuth.name, () => {
  it('Should return null', () => {
    const { result } = renderHook(() => useAuth(null))
    expect(result.current).toBeNull()
    expect(jwtDecode).not.toHaveBeenCalled()
  })

  it('Should return the decoded token', () => {
    const TOKEN = 'TOKEN'
    const EMAIL = 'EMAIL'
    const FIRSTNAME = 'FIRSTNAME'
    const LASTNAME = 'LASTNAME'

    const USER_TOKEN_DECODED_PAYLOAD: UserState = {
      token: TOKEN,
      email: EMAIL,
      firstname: FIRSTNAME,
      lastname: LASTNAME
    }
    const { result } = renderHook(() => useAuth(TOKEN))

    expect(jwtDecode).toHaveBeenCalledWith(TOKEN)
    expect(result.current).not.toBeNull()
    expect(result.current).toEqual(USER_TOKEN_DECODED_PAYLOAD)
  })
})