import jwtDecode from 'jwt-decode'
import { useEffect, useState } from 'react'
import { UserState } from '../contexts/user.context'

const useAuth = (token: string | null) => {
  const [payload, setPayload] = useState<UserState | null>(null)

  useEffect(() => {
    if(token) {
      const decoded = jwtDecode<UserState>(token)

      setPayload({
        token,
        firstname: decoded.firstname,
        lastname: decoded.lastname,
        email: decoded.email 
      })
    }

  }, [token])
  return payload
}

export default useAuth