import useTodos from './useTodos'
import { renderHook } from 'test.utils'
import { EXPECTED_TODOS, EXPECTED_TODOS_ERROR, USER } from '../mocks/handlers'
import { server } from '../mocks/server'
import { rest } from 'msw'

describe(useTodos.name, () => {
  it('Should fetch and return the todos', async () => {
    const fetch = jest.spyOn(window, 'fetch')
    const { result, waitForNextUpdate } = renderHook(() => useTodos(USER.token))

    await waitForNextUpdate()
    
    expect(result.current.todos).toEqual(EXPECTED_TODOS)
    expect(fetch).toHaveBeenCalled()
  })

  it('Should not fetch and return "undefined"', async () => {
    const fetch = jest.spyOn(window, 'fetch')

    const { result } = renderHook(() => useTodos(null))

    expect(result.current.todos).toBeUndefined()
    expect(fetch).not.toHaveBeenCalled()
  })

  it('Should not fetch the todos', async () => {
    server.use(
      rest.get('/api/todos', (req, res, ctx) => res(
          ctx.status(404),
          ctx.json(EXPECTED_TODOS_ERROR)
        )
      )
    )
    
    const fetch = jest.spyOn(window, 'fetch')
    const { result, waitForNextUpdate } = renderHook(() => useTodos(USER.token))
    await waitForNextUpdate()

    expect(result.current.todos).toBeUndefined()
    expect(result.current.error).toEqual(EXPECTED_TODOS_ERROR)
    expect(fetch).toHaveBeenCalled()

  })
})