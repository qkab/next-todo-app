import { render } from "test.utils";
import Navbar from ".";

const USER_NOT_CONNECTED = { user: { token: '', email: '', firstname: '', lastname: '' } }
const USER_CONNECTED = { user: { token: 'TOKEN', email: 'EMAIL', firstname: 'FIRSTNAME', lastname: 'LASTNAME' } }

describe(Navbar.name, () => {
    it('should have the Logo on the screen', () => {
        const providerProps = { value: USER_NOT_CONNECTED }
        const LOGO = 'LOGO'
        const { getByText } = render(<Navbar />, { providerProps })
        const logo = getByText(LOGO)

        expect(logo).toBeInTheDocument()
        expect(logo.innerHTML).toEqual(LOGO)
    })

    it('should have all three links', () => {
        const providerProps = { value: USER_NOT_CONNECTED }
        const EXPECTED_LINKS = ['Accueil', 'Inscription', 'Connexion']
        const { getAllByTestId } = render(<Navbar />, { providerProps })
        const links = getAllByTestId('links')
        const linkLabels = links.map((link) => link.innerHTML)

        expect(links).toHaveLength(3)
        expect(linkLabels).toEqual(EXPECTED_LINKS)
    })

    it('should have the name of the user connected', () => {
        const providerProps = { value: USER_CONNECTED }
        const { getByTestId } = render(<Navbar />, { providerProps })
        const user = getByTestId('username')

        expect(user).toHaveTextContent(`Salut ${USER_CONNECTED.user.firstname} ${USER_CONNECTED.user.lastname} !`)
    })
})