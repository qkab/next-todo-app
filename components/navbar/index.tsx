import Link from 'next/link'
import React, { useCallback, useContext } from 'react'
import { LogoutIcon } from '@heroicons/react/outline'

import { isUserConnected, UserContext } from '../../contexts/user.context'

const Navbar = () => {
  const { user, setUser } = useContext(UserContext)
  const logoutUser = useCallback(() => {
    localStorage.removeItem('token')
    setUser({ token: '', email: '', firstname: '', lastname: '' })
  }, [setUser])

  
  return (
    <div className='flex container mx-auto px-8 py-3 items-center'>
      <h1 className='flex-1'>LOGO</h1>
      <ul className='flex items-center space-x-5'>
        <li className='rounded-lg p-2 border-2 border-black hover:border-white hover:bg-black hover:text-white'><Link href={'/'}><a data-testid='links'>Accueil</a></Link></li>
        { !isUserConnected(user) ? (
          <>
            <li className='rounded-lg p-2 border-2 border-black hover:border-white hover:bg-black hover:text-white'><Link href={'/register'}><a data-testid='links'>Inscription</a></Link></li>
            <li className='rounded-lg p-2 border-2 border-black hover:border-white hover:bg-black hover:text-white'><Link href={'/login'}><a data-testid='links'>Connexion</a></Link></li>
          </>
        ) : (
          <li className='flex space-x-3'>
            <h3 data-testid='username'>Salut {`${user.firstname} ${user.lastname} `}!</h3>
            <button onClick={logoutUser}><LogoutIcon className='w-5 h-5'/></button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Navbar