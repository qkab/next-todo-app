import { createContext, Dispatch, ReactNode, SetStateAction, useMemo, useState } from "react";

const initialState = {
  token: '',
  email: '',
  firstname: '',
  lastname: '',
}

export type UserState = typeof initialState

export type UserContextType = {
  user: UserState
  setUser: Dispatch<SetStateAction<UserState>>
}

const isUserConnected = (user: UserState) => {
  return user.token !== '' && user.email !== '' && user.firstname !== '' && user.lastname !== ''
}

const UserContext = createContext<UserContextType>({ user: initialState, setUser: () => {}})


function UserProvider({ children }: { children: ReactNode }){
  const [user, setUser] = useState<UserState>(initialState)
  const userProviderValue = useMemo(() => ({ user, setUser }), [user, setUser])

  return <UserContext.Provider value={userProviderValue}>{children}</UserContext.Provider>
}

export { isUserConnected, UserContext, UserProvider }