import { server } from './mocks/server'
import { cleanup } from 'test.utils'
import '@testing-library/jest-dom/extend-expect'
import 'whatwg-fetch'

beforeAll(() => server.listen())
afterAll(() => server.close())
afterEach(() => {
  jest.restoreAllMocks()
  jest.clearAllMocks()
  server.resetHandlers()
  cleanup()
})