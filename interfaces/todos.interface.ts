export interface ITodo {
  title: string
  slug: string
  description: string
  completed: boolean
  user: string
}