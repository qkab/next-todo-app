import { rest } from 'msw'
import { UserState } from '../contexts/user.context'
import { ITodo } from '../interfaces/todos.interface'

export const USER: UserState = {
  token: 'TOKEN',
  email: 'EMAIL',
  firstname: 'FIRSTNAME',
  lastname: 'LASTNAME'
}

export const EXPECTED_TODOS: ITodo[] = [
  {
    title: 'TEST TITLE',
    description: 'DESCRIPTION',
    slug: 'TEST-TILE',
    completed: false,
    user: 'USER ID'
  }
]

export const EXPECTED_TODOS_ERROR = new Error("An error occurred while fetching the data.")

export const handlers = [
  rest.post('http://localhost:5000/api/v1/login', (req, res, ctx) => {
    req.headers.set('Authorization', USER.token)

    return res(
      ctx.status(200),
      ctx.json(USER.token)
    )
  }),
  rest.get('/api/todos', (req, res, ctx) => res(
    ctx.status(200),
    ctx.json(EXPECTED_TODOS)
  )),
  rest.get('*', (req, res, ctx) => {
    console.error(`Please add a request handler for ${req.url.toString()}`);
    return res(
      ctx.status(500),
      ctx.json({ error: 'Please add a request handler '})
    )
  })
]