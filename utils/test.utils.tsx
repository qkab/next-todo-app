import { ReactElement, ReactNode } from 'react'
import { render, RenderOptions } from '@testing-library/react'
import { UserContext } from '../contexts/user.context'
import { renderHook } from '@testing-library/react-hooks'
import { SWRConfig } from 'swr'

const SWRConfigProvider = ({ children }: { children: ReactNode }) => (
  <SWRConfig value={{ dedupingInterval: 0, provider: () => new Map() }}>
    {children}
  </SWRConfig>
)

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'> | any) => render(
  <UserContext.Provider {...options.providerProps}>{ui}</UserContext.Provider>, options,
)

function customRenderHook<T>(ui: () => T){
  return renderHook(ui, { wrapper: SWRConfigProvider })
}

export * from '@testing-library/react'
export { customRender as render }
export { customRenderHook as renderHook }