// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

import { ITodo } from '../../../interfaces/todos.interface'

type Data = {
  todos: ITodo[]
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const { data: todos } = await axios.get('http://localhost:5000/api/v1/todos', { headers: { 'Authorization': req.headers.authorization! }})

  res.status(200).json(todos)
}
