import { render, fireEvent } from "test.utils";
import Register from "./index";

describe(Register.name, () => {
  const providerProps = { providerProps: {} }

  it('should display the register form', () => {
    const { getByTestId, getAllByTestId } = render(<Register />, { providerProps })
    const form = getByTestId('register-form')
    const inputs = getAllByTestId('register-form-input')
    
    const inputsIds = inputs.map((input) => input.id)
    const EXPECTED_INPUT_IDS = ['lastname', 'firstname', 'email', 'password', 'confirmPassword']

    expect(form).toBeInTheDocument()
    expect(inputs).toHaveLength(5)    
    expect(inputsIds).toHaveLength(5)
    expect(inputsIds).toEqual(EXPECTED_INPUT_IDS)
  })

  it('should register a player', () => {
    const { getByTestId, getByLabelText } = render(<Register />, { providerProps })

    const lastnameInput = getByLabelText('lastname') as HTMLInputElement
    const firstnameInput = getByLabelText('firstname') as HTMLInputElement
    const emailInput = getByLabelText('email') as HTMLInputElement
    const passwordInput = getByLabelText('password') as HTMLInputElement
    const confirmPasswordInput = getByLabelText('confirmPassword') as HTMLInputElement
    const registerSubmitInput = getByTestId('register-submit') as HTMLInputElement

    expect(lastnameInput).toBeInTheDocument()
    expect(firstnameInput).toBeInTheDocument()
    expect(emailInput).toBeInTheDocument()
    expect(passwordInput).toBeInTheDocument()
    expect(confirmPasswordInput).toBeInTheDocument()
    expect(registerSubmitInput).toBeInTheDocument()

    fireEvent.change(lastnameInput, { target: { value: 'Doe' } })
    fireEvent.change(firstnameInput, { target: { value: 'John' } })
    fireEvent.change(emailInput, { target: { value: 'john.doe@gmail.com' } })
    fireEvent.change(passwordInput, { target: { value: 'validPassword' } })
    fireEvent.change(confirmPasswordInput, { target: { value: 'validPassword' } })

    expect(lastnameInput.value).toBe('Doe')
    expect(firstnameInput.value).toBe('John')
    expect(emailInput.value).toBe('john.doe@gmail.com')
    expect(passwordInput.value).toBe('validPassword')
    expect(confirmPasswordInput.value).toBe('validPassword')

    // Click on the submit btn
  })

  it.todo('should display an error message')
})