import { useRouter } from 'next/router'
import React, { useState } from 'react'
import toast from 'react-hot-toast'

const Register = () => {
  const router = useRouter()

  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const payload = { firstname, lastname, email, password, confirmPassword }

    try {
      const res = await fetch('http://localhost:5000/api/v1/users/register', {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: { 'Content-Type': 'application/json' }
      })
      const data = await res.json()

      if(res.status === 400 || res.status === 500) {
        toast.error(data.message)
      } else {
        toast.success('Inscription réussie !')
        router.push('/login')
      }
    } catch (error: any) {
      toast.error(error.message)
    }
  }

  return (
    <div className='flex items-center justify-center h-screen w-full'>
      <form data-testid='register-form' className='space-y-3' onSubmit={handleSubmit}>
        <div>
          <label className='block' htmlFor="lastname">Nom</label>
          <input data-testid='register-form-input' className='form-input' type="text" aria-label='lastname' id='lastname' placeholder='Nom' value={lastname} onChange={(e) => setLastname(e.target.value)}/>
        </div>

        <div>
          <label className='block' htmlFor="firstname">Prénom</label>
          <input data-testid='register-form-input' className='form-input' type="text" aria-label='firstname' id='firstname' placeholder='Prénom' value={firstname} onChange={(e) => setFirstname(e.target.value)}/>
        </div>

        <div>
          <label className='block' htmlFor="email">Email</label>
          <input data-testid='register-form-input' className='form-input' type="email" aria-label='email' id='email' placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)}/>
        </div>

        <div>
          <label className='block' htmlFor="password">Mot de passe</label>
          <input data-testid='register-form-input' className='form-input' type="password" aria-label='password' id='password' placeholder='Mot de passe' value={password} onChange={(e) => setPassword(e.target.value)}/>
        </div>

        <div>
          <label className='block' htmlFor="confirmPassword">Confirmation</label>
          <input data-testid='register-form-input' className='form-input' type="password" aria-label='confirmPassword' id='confirmPassword' placeholder='Mot de passe' value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)}/>
        </div>

        <button data-testid='register-submit' type='submit' className='rounded-lg bg-black w-full p-2 text-white'>S'inscrire</button>
      </form>
    </div>
  )
}

export default Register