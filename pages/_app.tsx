import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Navbar from '../components/navbar'
import { Toaster } from 'react-hot-toast'
import { UserProvider } from '../contexts/user.context'

function MyApp({ Component, pageProps }: AppProps) {


  return (
    <>
      <UserProvider>
        <Navbar />
        <Component {...pageProps} />
        <Toaster />
      </UserProvider>
    </>
  )
}

export default MyApp
