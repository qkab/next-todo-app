import { useRouter } from 'next/router'
import React, { useState } from 'react'
import toast from 'react-hot-toast'

const Login = () => {
  const router = useRouter()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const payload = { email, password }

    try {
      const res = await fetch('http://localhost:5000/api/v1/users/login', {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: { 'Content-Type': 'application/json' }
      })
      const data = await res.json()
      
      if(res.status === 404 || res.status === 400) {
        toast.error(data.message)
      } else {
        toast.success('Connexion réussie !')
        localStorage.setItem('token', data.token)
        router.push('/')
      }
    } catch (error: any) {
      toast.error(error.message)
    }
  }

  return (
    <div className='flex items-center justify-center h-screen w-full'>
      <form className='space-y-3' onSubmit={handleSubmit}>
        <div>
          <label className='block' htmlFor="email">Email</label>
          <input className='form-input' type="email" id='email' placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)}/>
        </div>

        <div>
          <label className='block' htmlFor="password">Mot de passe</label>
          <input className='form-input' type="password" id='password' placeholder='Mot de passe' value={password} onChange={(e) => setPassword(e.target.value)}/>
        </div>

        <button type='submit' className='rounded-lg bg-black w-full p-2 text-white'>Se connecter</button>
      </form>
    </div>
  )
}

export default Login